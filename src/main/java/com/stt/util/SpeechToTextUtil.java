package com.stt.util;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Locale;
import java.util.ResourceBundle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;

public class SpeechToTextUtil {

	private static final Logger LOGGER = LoggerFactory.getLogger(SpeechToTextUtil.class);

	private static final ResourceBundle APP_PROPS = ResourceBundle.getBundle("application", Locale.ENGLISH);

	public static final Path UPLOAD_TMP_DIR = Paths.get(APP_PROPS.getString("app.temp.dir")).toAbsolutePath().normalize();

	public static final Charset CHARSET_UTF8 = Charset.forName("utf8");

	public static final MediaType APPLICATION_JSON_UTF8 = new MediaType(
			MediaType.APPLICATION_JSON.getType(),
			MediaType.APPLICATION_JSON.getSubtype(),
			CHARSET_UTF8);

	public static final boolean SKIP_RECOGNITION = Boolean.getBoolean("skipRecognition");

	/**
	 * 
	 * @param filename
	 * @param bytes
	 * @return
	 * @throws IOException
	 */
	public static Path storeFileIntoDisk(String filename, byte[] bytes) throws IOException {
		String newFilename = System.currentTimeMillis() + "-" + filename;
		Path path = SpeechToTextUtil.UPLOAD_TMP_DIR.resolve(newFilename);
		// create the parent directories
		if (Files.notExists(path.getParent())) {
			Files.createDirectories(path.getParent());
		}
		// create the file on server
		try (BufferedOutputStream stream = new BufferedOutputStream(Files.newOutputStream(path))) {
			stream.write(bytes);
		}
		return path;
	}

	/**
	 * 
	 * @param speechName
	 * @param speechFile
	 * @return
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public static Path resampleToWav(String speechName, byte[] speechFile) throws IOException, InterruptedException {
		LOGGER.debug("Saving file: " + speechName + ".wav");
		Path path = SpeechToTextUtil.storeFileIntoDisk(speechName + ".wav", speechFile);
		Path wavFile = path.getParent().resolve(System.currentTimeMillis() + "-" + speechName + "-resample.wav");

		String command = "ffmpeg -i \"" + path + "\" -acodec pcm_s16le -ac 1 -ar 16000 \"" + wavFile + "\"";
		LOGGER.debug("Executing: {}", command);

		Process proc = Runtime.getRuntime().exec(command);

		StringBuilder stderr = new StringBuilder(1024);
		try (BufferedReader br = new BufferedReader(new InputStreamReader(proc.getErrorStream()))) {
			String line;
			while ((line = br.readLine()) != null) {
				stderr.append(line).append("\n");
			}
		}

		int exitValue = proc.waitFor();
		LOGGER.info("ffmpeg returned value: " + exitValue);

		if (exitValue != 0) {
			LOGGER.error("ffmpeg : {}", stderr.toString());
		}

		return wavFile;
	}

}
