package com.stt.util;

public class WebSocketToken {

	public static final String CREATE_RECOGNIZER = "###CREATE###";
	public static final String RECOGNIZER_READY = "###READY###";
	public static final String RECOGNITION_END = "###END###";
	public static final String SESSION_ID = "###ID###";

}
