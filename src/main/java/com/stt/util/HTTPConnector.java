package com.stt.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.Proxy;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;
import java.util.zip.Inflater;
import java.util.zip.InflaterInputStream;

import javax.xml.bind.DatatypeConverter;

/**
 * @author ciri-cuervo
 * @version 1.2
 */
public class HTTPConnector {

	private static final int IO_BUFFER_SIZE = 8 * 1024; // 8 KiB
	private static final int GZIP_BUFFER_SIZE = 4 * 1024; // 4 KiB
	private static final int MIN_GZIP_SIZE = 256; // bytes

	// most popular content types
	public static final String CONTENT_TYPE_ANY = "*/*";
	public static final String CONTENT_TYPE_BINARY = "application/octet-stream";
	public static final String CONTENT_TYPE_HTML = "text/html";
	public static final String CONTENT_TYPE_JSON = "application/json";
	public static final String CONTENT_TYPE_TEXT = "text/plain";
	public static final String CONTENT_TYPE_URLENC = "application/x-www-form-urlencoded";
	public static final String CONTENT_TYPE_XML = "application/xml";

	// most popular encodings
	public static final String CHARSET_ASCII = "US-ASCII";
	public static final String CHARSET_CHINESE_SIMPLIFIED = "GB2312";
	public static final String CHARSET_CHINESE_TRADITIONAL = "Big5";
	public static final String CHARSET_JAPANESE = "Shift_JIS";
	public static final String CHARSET_LATIN_1 = "ISO-8859-1";
	public static final String CHARSET_LATIN_2 = "ISO-8859-2";
	public static final String CHARSET_UNICODE = "UTF-8";
	public static final String CHARSET_WINDOWS = "windows-1252";
	public static final String CHARSET_WINDOWS_CYRILLIC = "windows-1251";

	// HTTP properties
	public static final int PROP_COMPRESS_REQUEST_CONTENT = 1 << 0;
	public static final int PROP_ENABLE_CACHING = 1 << 1;
	public static final int PROP_DISABLE_KEEP_ALIVE = 1 << 2;
	public static final int PROP_SEND_CHUNKED = 1 << 3; // TODO implement chunks sending

	// request method enum
	private enum RequestMethod {
		GET, POST, PUT, DELETE;
	}

	private HTTPConnector() {
	}

	/*
	 * --------------------- HTTP REQUEST CALLS ---------------------
	 */

	/**
	 * 
	 * @param url
	 * @return
	 * @throws MalformedURLException
	 */
	public static HTTPRequest get(String url) throws MalformedURLException {
		return new HTTPRequest(RequestMethod.GET, url);
	}

	/**
	 * 
	 * @param url
	 * @return
	 * @throws MalformedURLException
	 */
	public static HTTPOutputRequest post(String url) throws MalformedURLException {
		return new HTTPOutputRequest(RequestMethod.POST, url);
	}

	/**
	 * 
	 * @param url
	 * @return
	 * @throws MalformedURLException
	 */
	public static HTTPOutputRequest put(String url) throws MalformedURLException {
		return new HTTPOutputRequest(RequestMethod.PUT, url);
	}

	/**
	 * 
	 * @param url
	 * @return
	 * @throws MalformedURLException
	 */
	public static HTTPRequest delete(String url) throws MalformedURLException {
		return new HTTPRequest(RequestMethod.DELETE, url);
	}

	/*
	 * --------------------- HTTP REQUEST CLASSES ---------------------
	 */

	public static class HTTPRequest {

		protected RequestMethod requestMethod;

		protected URL url;
		protected Map<String, String> headers = new HashMap<>();
		protected Proxy proxy;
		protected int timeout;
		protected int props;

		protected HttpURLConnection urlConn;
		protected ByteArrayOutputStream response;

		/**
		 * 
		 * @param requestMethod
		 * @param url
		 * @throws MalformedURLException
		 */
		protected HTTPRequest(RequestMethod requestMethod, String url) throws MalformedURLException {
			this.requestMethod = requestMethod;
			this.url = new URL(url);
		}

		/**
		 * 
		 * @param name
		 * @param value
		 * @return
		 */
		public HTTPRequest setHeader(String name, String value) {
			this.headers.put(name, value);
			return this;
		}

		/**
		 * 
		 * @param user
		 * @param pass
		 * @return
		 */
		public HTTPRequest setBasicAuth(String user, String pass) {
			String auth = "Basic " + DatatypeConverter.printBase64Binary((user + ":" + pass).getBytes());
			setHeader("Authorization", auth);
			return this;
		}

		/**
		 * 
		 * @param proxyHost
		 * @param proxyPort
		 * @return
		 */
		public HTTPRequest setProxy(String proxyHost, int proxyPort) {
			this.proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(proxyHost, proxyPort));
			return this;
		}

		/**
		 * 
		 * @param user
		 * @param pass
		 * @return
		 */
		public HTTPRequest setProxyBasicAuth(String user, String pass) {
			String auth = "Basic " + DatatypeConverter.printBase64Binary((user + ":" + pass).getBytes());
			setHeader("Proxy-Authorization", auth);
			return this;
		}

		/**
		 * 
		 * @param props
		 * @return
		 */
		public HTTPRequest setProps(int props) {
			this.props = props;
			return this;
		}

		/**
		 * 
		 * @param timeoutInMillis
		 * @return
		 * @throws HTTPConnectorException
		 * @throws IOException
		 */
		public byte[] executeByteArray(int timeoutInMillis) throws HTTPConnectorException, IOException {
			this.timeout = timeoutInMillis;
			execute();
			return response.toByteArray();
		}

		/**
		 * 
		 * @param timeoutInMillis
		 * @param charset
		 * @return
		 * @throws HTTPConnectorException
		 * @throws IOException
		 */
		public String executeString(int timeoutInMillis, String charset) throws HTTPConnectorException, IOException {
			this.timeout = timeoutInMillis;
			setHeader("Accept-Charset", charset);
			execute();
			return response.toString(charset);
		}

		/**
		 * 
		 * @throws IOException
		 */
		private void execute() throws IOException {

			try {
				if (proxy == null) {
					urlConn = (HttpURLConnection) url.openConnection();
				} else {
					urlConn = (HttpURLConnection) url.openConnection(proxy);
				}

				// set request method
				urlConn.setRequestMethod(requestMethod.name());

				// prepare connection properties
				urlConn.setConnectTimeout(timeout);
				urlConn.setReadTimeout(timeout);

				urlConn.setRequestProperty("User-Agent",
						"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.125 Safari/537.36");
				urlConn.setRequestProperty("Accept", "*/*");
				urlConn.setRequestProperty("Accept-Encoding", "gzip, deflate");

				// check active flags
				if ((props & PROP_ENABLE_CACHING) == 0) {
					urlConn.setUseCaches(false);
					urlConn.setRequestProperty("Cache-Control", "no-cache, no-store, max-age=0, must-revalidate");
				}

				if ((props & PROP_DISABLE_KEEP_ALIVE) != 0) {
					urlConn.setRequestProperty("Connection", "close");
				} else {
					urlConn.setRequestProperty("Connection", "keep-alive");
				}

				// set HTTP header
				setCustomHeaders();

				// send request data
				sendData();

				// receive the response
				receiveResponse();

				if (urlConn.getResponseCode() >= 400) {
					throw new HTTPConnectorException(urlConn, response);
				}
			} finally {
				if (urlConn != null) {
					urlConn.disconnect();
				}
			}
		}

		/**
		 * 
		 */
		protected void setCustomHeaders() {
			if (headers != null && headers.size() > 0) {
				for (Entry<String, String> header : headers.entrySet()) {
					urlConn.setRequestProperty(header.getKey(), header.getValue());
				}
				headers.clear();
			}
		}

		/**
		 * 
		 * @throws IOException
		 */
		protected void sendData() throws IOException {
			// do nothing
		}

		/**
		 * 
		 * @throws IOException
		 */
		protected void receiveResponse() throws IOException {
			InputStream is = null;
			try {

				if (urlConn.getResponseCode() >= 400) {
					is = urlConn.getErrorStream();
				} else {
					is = urlConn.getInputStream();
				}

				if ("gzip".equalsIgnoreCase(urlConn.getContentEncoding())) {
					is = new GZIPInputStream(is, GZIP_BUFFER_SIZE);
				} else if ("deflate".equalsIgnoreCase(urlConn.getContentEncoding())) {
					is = new InflaterInputStream(is, new Inflater(true), GZIP_BUFFER_SIZE);
				}

				is = new BufferedInputStream(is, IO_BUFFER_SIZE);

				response = new ByteArrayOutputStream(IO_BUFFER_SIZE);
				copy(is, response, IO_BUFFER_SIZE);

			} finally {
				close(is);
			}
		}

	}

	public static class HTTPOutputRequest extends HTTPRequest {

		protected byte[] data;

		/**
		 * 
		 * @param requestMethod
		 * @param url
		 * @throws MalformedURLException
		 */
		protected HTTPOutputRequest(RequestMethod requestMethod, String url) throws MalformedURLException {
			super(requestMethod, url);
		}

		/**
		 * 
		 * @param data
		 * @param contentType
		 * @return
		 */
		public HTTPOutputRequest setOutput(byte[] data, String contentType) {
			this.data = data;
			setHeader("Content-Type", contentType);
			return this;
		}

		/**
		 * 
		 * @param data
		 * @param contentType
		 * @param charset
		 * @return
		 * @throws UnsupportedEncodingException
		 */
		public HTTPOutputRequest setOutput(String data, String contentType, String charset) throws UnsupportedEncodingException {
			this.data = data.getBytes(charset);
			setHeader("Content-Type", contentType + "; charset=" + charset);
			return this;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see com.stt.util.HTTPConnector.HTTPRequest#sendData()
		 */
		@Override
		protected void sendData() throws IOException {

			urlConn.setDoOutput(true);

			if (data == null) {
				return; // nothing to send
			}

			// only if data length is higher than the minimum required
			boolean compressData = (props & PROP_COMPRESS_REQUEST_CONTENT) != 0 && data.length > MIN_GZIP_SIZE;

			if (compressData) {
				// set request property
				urlConn.setRequestProperty("Content-Encoding", "gzip");
			}

			// send the request
			OutputStream os = null;
			try {

				os = urlConn.getOutputStream();

				if (compressData) {
					os = new GZIPOutputStream(os, GZIP_BUFFER_SIZE);
				}

				os = new BufferedOutputStream(os, IO_BUFFER_SIZE);

				copy(new ByteArrayInputStream(data), os, IO_BUFFER_SIZE);

			} finally {
				close(os);
			}
		}

	}

	/*
	 * --------------------- UTIL METHODS ---------------------
	 */

	/**
	 * 
	 * @param c
	 */
	private static void close(Closeable c) {
		if (c == null)
			return;
		try {
			c.close();
		} catch (IOException e) {
		}
	}

	/**
	 * 
	 * @param input
	 * @param output
	 * @param bufferSize
	 * @throws IOException
	 */
	private static void copy(InputStream input, OutputStream output, int bufferSize) throws IOException {
		byte[] buffer = new byte[bufferSize];
		int n = 0;
		while (-1 != (n = input.read(buffer))) {
			output.write(buffer, 0, n);
		}
	}

	/*
	 * --------------------- EXCEPTIONS ---------------------
	 */

	public static class HTTPConnectorException extends IOException {

		private static final long serialVersionUID = 1L;

		protected int responseCode;
		protected String responseMessage;
		protected ByteArrayOutputStream response;

		/**
		 * 
		 * @param urlConn
		 */
		public HTTPConnectorException(HttpURLConnection urlConn, ByteArrayOutputStream response) {
			super(urlConn.getHeaderField(0));

			try {
				this.responseCode = urlConn.getResponseCode();
				this.responseMessage = urlConn.getResponseMessage();
			} catch (IOException e) {
				this.responseCode = -1;
				this.responseMessage = null;
			}

			this.response = response;
		}

		/**
		 * 
		 * @return
		 */
		public int getResponseCode() {
			return responseCode;
		}

		/**
		 * 
		 * @return
		 */
		public String getResponseMessage() {
			return responseMessage;
		}

		/**
		 * 
		 * @return
		 */
		public byte[] getResponseBodyByteArray() {
			return response.toByteArray();

		}

		/**
		 * 
		 * @return
		 */
		public String getResponseBodyString() {
			return response.toString();
		}

		/**
		 * 
		 * @param charset
		 * @return
		 * @throws UnsupportedEncodingException
		 */
		public String getResponseBodyString(String charset) throws UnsupportedEncodingException {
			return response.toString(charset);
		}

	}

	/*
	 * --------------------- PARAMETERS ENCODING ---------------------
	 */

	/**
	 * 
	 * @param params
	 * @param encoding
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	public static final String encodeParams(Map<String, String> params, String encoding) throws UnsupportedEncodingException {
		StringBuilder encodedParams = new StringBuilder(2048);

		for (Entry<String, String> param : params.entrySet()) {

			if (encodedParams.length() > 0) {
				encodedParams.append('&');
			}

			encodedParams.append(URLEncoder.encode(param.getKey(), encoding));
			encodedParams.append('=');
			encodedParams.append(URLEncoder.encode(param.getValue(), encoding));
		}

		return encodedParams.toString();
	}

}
