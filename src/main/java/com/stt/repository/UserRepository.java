package com.stt.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.stt.model.User;

public interface UserRepository extends JpaRepository<User, Long> {

	User findByUsername(String username);

}
