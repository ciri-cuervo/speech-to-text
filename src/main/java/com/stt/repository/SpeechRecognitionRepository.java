package com.stt.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.stt.model.SpeechRecognition;

public interface SpeechRecognitionRepository extends JpaRepository<SpeechRecognition, Long> {

}
