package com.stt.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.stt.model.Note;

public interface NoteRepository extends JpaRepository<Note, Long> {

	@Query("SELECT n FROM Note n where n.user.id = ?1")
	List<Note> findByUserId(Long userId);

}
