package com.stt.service.responsehandler;

public interface ResponseHandler {

	public void sendResponse(String message);

	public void transmissionEnd();

	public void close();

}
