package com.stt.service.responsehandler;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

import com.stt.util.WebSocketToken;

/**
 * This {@link ResponseHandler} sends web socket messages asynchronously.<br>
 * It saves every message into a thread-pool to process them in a future.
 * 
 */
public class WebSocketResponseHandler implements ResponseHandler {

	private WebSocketSession session;
	private ExecutorService executorService;

	public WebSocketResponseHandler(WebSocketSession session) {
		this.session = session;
		this.executorService = Executors.newFixedThreadPool(1);
	}

	@Override
	public void sendResponse(String message) {
		executorService.execute(new ResponseHandlerTask(session, message));
	}

	@Override
	public void transmissionEnd() {
		executorService.execute(new ResponseHandlerTask(session, WebSocketToken.RECOGNITION_END));
	}

	@Override
	public void close() {
		executorService.shutdownNow();
	}

	/**
	 * 
	 */
	private static class ResponseHandlerTask implements Runnable {

		private static final Logger LOGGER = LoggerFactory.getLogger(ResponseHandlerTask.class);

		private WebSocketSession session;
		private String message;

		public ResponseHandlerTask(WebSocketSession session, String message) {
			this.session = session;
			this.message = message;
		}

		@Override
		public void run() {
			try {
				if (session.isOpen()) {
					LOGGER.debug("sending web socket message - message: \"{}\" - sessionID: {}", message, session.getId());
					session.sendMessage(new TextMessage(message));
				} else {
					LOGGER.warn("couldn't send web socket message - Reason: session is closed. - sessionID: {}", session.getId());
				}
			} catch (Exception e) {
				LOGGER.error("An error ocurred while sending message: " + message, e);
			}
		}
	}

}
