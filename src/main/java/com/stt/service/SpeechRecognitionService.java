package com.stt.service;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Date;
import java.util.concurrent.RejectedExecutionException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.stt.model.SpeechRecognition;
import com.stt.repository.SpeechRecognitionRepository;
import com.stt.service.responsehandler.ResponseHandler;
import com.stt.util.SpeechToTextUtil;

import edu.cmu.sphinx.api.Configuration;
import edu.cmu.sphinx.api.SpeechResult;

public class SpeechRecognitionService {

	private static final String SENTENCE_SEPARATOR = "\n";

	private static final Logger LOGGER = LoggerFactory.getLogger(SpeechRecognitionService.class);

	@Autowired
	private SpeechRecognitionRepository speechRecognitionRepository;

	private ConfigurableStreamSpeechRecognizer speechRecognizer;
	private ResponseHandler responseHandler;

	@Autowired
	public SpeechRecognitionService(String sphinxConfigFile, Configuration configuration, ResponseHandler responseHandler)
			throws IOException {
		this.speechRecognizer = new ConfigurableStreamSpeechRecognizer(sphinxConfigFile, configuration);
		this.responseHandler = responseHandler;
	}

	public ConfigurableStreamSpeechRecognizer getSpeechRecognizer() {
		return speechRecognizer;
	}

	public ResponseHandler getResponseHandler() {
		return responseHandler;
	}

	public void setResponseHandler(ResponseHandler responseHandler) {
		this.responseHandler = responseHandler;
	}

	/**
	 * 
	 * @param speechName
	 * @param speechFile
	 * @return
	 * @throws IOException
	 */
	public SpeechRecognition recognize(Path speechFile) throws IOException {

		// store recognition in DB
		Date now = new Date();
		SpeechRecognition recognition = new SpeechRecognition();
		recognition.setCreated(now);
		recognition.setSpeechFilename(speechFile.getFileName().toString());
		recognition.setSpeechPath(speechFile.toString());
		recognition = speechRecognitionRepository.save(recognition);

		StringBuilder text = new StringBuilder(256);
		try {
			// open file stream for speech recognition
			try (InputStream stream = new BufferedInputStream(Files.newInputStream(speechFile))) {
				SpeechResult result;

				if (!SpeechToTextUtil.SKIP_RECOGNITION) {
					speechRecognizer.startRecognition(stream);
					while ((result = speechRecognizer.getResult()) != null) {
						String hypothesis = result.getHypothesis();

						// send new hypothesis
						responseHandler.sendResponse(hypothesis);

						if (text.length() > 0) {
							text.append(SENTENCE_SEPARATOR);
						}
						text.append(hypothesis);

						LOGGER.debug("Recognized sentence: {}", hypothesis);
					}
					speechRecognizer.stopRecognition();
				}

			} finally {
				// notify recognition end
				responseHandler.transmissionEnd();
			}

		} catch (RejectedExecutionException e) {
			recognition.setError(e.getMessage());
			LOGGER.warn("Recognition was aborted because response handler rejected the operation.");
		} catch (IOException | RuntimeException e) {
			recognition.setError(e.getMessage());
			throw e;
		} finally {
			// update recognition details
			recognition.setText(text.toString());
			recognition = speechRecognitionRepository.save(recognition);
		}

		LOGGER.info("Recognized speech: {}", recognition.getText());
		LOGGER.info("Speech path: {}", recognition.getSpeechPath());

		// return the recognition data
		return recognition;
	}

}
