package com.stt.service;

import java.nio.file.Path;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ForkJoinPool;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.socket.WebSocketSession;

import com.stt.util.WebSocketToken;

/**
 * It is charge of creating a new {@literal SpeechRecognizer} per web socket session.<br>
 * It also ensures a 'per session' serial execution of tasks.
 * 
 */
public class SpeechRecognizerManager {

	private static final Logger LOGGER = LoggerFactory.getLogger(SpeechRecognizerManager.class);
	private static final int CLEANING_INTERVAL = 300000; // 300 seconds

	static {
		new SpeechRecognizerCleanner().start();
	}

	private static Map<String, SpeechRecognitionUnit> speechRecognizers = new ConcurrentHashMap<>();

	public static void createSpeechRecognizer(WebSocketSession session, SpeechRecognitionService service) {
		SpeechRecognitionUnit newUnit = new SpeechRecognitionUnit(session, service);
		SpeechRecognitionUnit oldUnit = speechRecognizers.put(session.getId(), newUnit);
		SpeechRecognitionUnit.notifyReady(newUnit);
		SpeechRecognitionUnit.stopSilently(oldUnit);
	}

	public static void deleteSpeechRecognizer(String sessionId) {
		SpeechRecognitionUnit unit = speechRecognizers.remove(sessionId);
		if (unit != null) {
			SpeechRecognitionUnit.stopSilently(unit);
			LOGGER.debug("delete speech recognizer - sessionId: {}", unit.session.getId());
		}
	}

	public static boolean execute(String sessionId, Path speechFile) {
		SpeechRecognitionUnit unit = speechRecognizers.get(sessionId);
		if (unit == null) return false;
		unit.oneProcessPool.execute(new SpeechRecognitionTask(unit.service, speechFile));
		return true;
	}

	public static int getRecognizerCount() {
		return speechRecognizers.size();
	}

	public static int getQueuedTaskCount(String sessionId) {
		SpeechRecognitionUnit unit = speechRecognizers.get(sessionId);
		return (unit == null ? 0 : unit.oneProcessPool.getQueuedSubmissionCount());
	}

	/**
	 * 
	 */
	private static class SpeechRecognitionUnit {

		private WebSocketSession session;
		private SpeechRecognitionService service;
		private ForkJoinPool oneProcessPool;

		public SpeechRecognitionUnit(WebSocketSession session, SpeechRecognitionService service) {
			this.session = session;
			this.service = service;
			this.oneProcessPool = new ForkJoinPool(1);
		}

		public static void notifyReady(SpeechRecognitionUnit unit) {
			if (unit != null) {
				unit.service.getResponseHandler().sendResponse(WebSocketToken.RECOGNIZER_READY);
			}
		}

		private static void stopSilently(SpeechRecognitionUnit unit) {
			if (unit == null) {
				return;
			}
			try {
				unit.service.getSpeechRecognizer().stopRecognition();
			} catch (Exception e) {
				// nothing
			}
			try {
				unit.service.getResponseHandler().close();
			} catch (Exception e) {
				// nothing
			}
			try {
				unit.oneProcessPool.shutdownNow();
			} catch (Exception e) {
				// nothing
			}
		}
	}

	/**
	 * 
	 */
	private static class SpeechRecognitionTask implements Runnable {

		private SpeechRecognitionService service;
		private Path speechFile;

		public SpeechRecognitionTask(SpeechRecognitionService service, Path speechFile) {
			this.service = service;
			this.speechFile = speechFile;
		}

		@Override
		public void run() {
			try {
				service.recognize(speechFile);
			} catch (Exception e) {
				LOGGER.error("There was error while recognizing: " + speechFile, e);
			}
		}
	}

	/**
	 * 
	 */
	private static class SpeechRecognizerCleanner extends Thread {

		public SpeechRecognizerCleanner() {
			super("speech-recognizer-cleaner");
			setDaemon(true); // not to prevent the JVM from exiting when the program finishes
		}

		@Override
		public void run() {
			try {
				while (true) {
					Thread.sleep(CLEANING_INTERVAL);
					for (SpeechRecognitionUnit unit : SpeechRecognizerManager.speechRecognizers.values()) {
						if (!unit.session.isOpen()) {
							deleteSpeechRecognizer(unit.session.getId());
						}
					}
					System.gc();
				}
			} catch (Exception e) {
				LOGGER.error("There was error while cleanig the speech recognizers", e);
			}
		}
	}

}
