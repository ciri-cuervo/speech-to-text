package com.stt.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class SpeechRecognition {

	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	protected Long id;
	@Column(length = 2000)
	protected String text;
	@Column(length = 1000)
	protected String error;
	@ManyToOne
	protected User user;
	@Temporal(TemporalType.TIMESTAMP)
	protected Date created;
	protected String speechFilename;
	protected String speechPath;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public String getSpeechFilename() {
		return speechFilename;
	}

	public void setSpeechFilename(String speechFilename) {
		this.speechFilename = speechFilename;
	}

	public String getSpeechPath() {
		return speechPath;
	}

	public void setSpeechPath(String speechPath) {
		this.speechPath = speechPath;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (id == null || obj == null || getClass() != obj.getClass())
			return false;
		return id.equals(((SpeechRecognition) obj).id);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return id == null ? 0 : id.hashCode();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "SpeechRecognition [id=" + id + ", text=" + text + ", user=" + user + ", created=" + created + ", speechFilename="
				+ speechFilename + ", speechPath=" + speechPath + "]";
	}

}
