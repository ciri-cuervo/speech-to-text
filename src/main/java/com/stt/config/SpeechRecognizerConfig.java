package com.stt.config;

import java.io.IOException;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;

import com.stt.service.SpeechRecognitionService;
import com.stt.service.responsehandler.ResponseHandler;

import edu.cmu.sphinx.api.Configuration;

@org.springframework.context.annotation.Configuration
public class SpeechRecognizerConfig {

	@Bean
	public Configuration speechRecognizerConfiguration() {

		Configuration configuration = new Configuration();

		// Set path to acoustic model.
		configuration.setAcousticModelPath("resource:/com/stt/models/acoustic/en-us");
		// Set path to dictionary.
		configuration.setDictionaryPath("resource:/com/stt/models/acoustic/en-us/dict/cmu07a.dic");
		// Set language model.
		configuration.setLanguageModelPath("resource:/com/stt/models/language/en-us.lm.dmp");

		return configuration;
	}

	@Bean
	@Scope("prototype")
	public SpeechRecognitionService speechRecognitionService(ResponseHandler responseHandler) throws IOException {
		return new SpeechRecognitionService("resource:/com/stt/config/sphinx4.config.xml", speechRecognizerConfiguration(), responseHandler);
	}

}
