package com.stt.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;
import org.springframework.web.socket.server.standard.ServletServerContainerFactoryBean;
import org.springframework.web.socket.server.support.HttpSessionHandshakeInterceptor;

import com.stt.controller.SpeechRecognitionHandler;

@Configuration
@EnableWebSocket
public class WebSocketConfig implements WebSocketConfigurer {

	private static final int MAX_BUFFER_SIZE = 512 * 1024; // 512 Kb

	@Override
	public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
		registry.addHandler(speechRecognitionHandler(), "/speechrecognition")
			.addInterceptors(new HttpSessionHandshakeInterceptor())
			.withSockJS();
	}

	@Bean
	public WebSocketHandler speechRecognitionHandler() {
		return new SpeechRecognitionHandler();
	}

	@Bean
	public ServletServerContainerFactoryBean createServletServerContainerFactoryBean() {
	    ServletServerContainerFactoryBean container = new ServletServerContainerFactoryBean();
	    container.setMaxTextMessageBufferSize(MAX_BUFFER_SIZE);
	    container.setMaxBinaryMessageBufferSize(MAX_BUFFER_SIZE);
	    return container;
	}

}
