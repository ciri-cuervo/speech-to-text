package com.stt.config;

import java.io.IOException;
import java.nio.file.Files;

import javax.servlet.Filter;
import javax.servlet.MultipartConfigElement;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

import org.slf4j.bridge.SLF4JBridgeHandler;
import org.springframework.web.filter.CharacterEncodingFilter;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

import com.stt.util.SpeechToTextUtil;

public class WebMvcInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

	@Override
	protected Class<?>[] getRootConfigClasses() {
		return new Class[] { PersistenceConfig.class, SecurityConfig.class, SpeechRecognizerConfig.class, WebSocketConfig.class };
	}

	@Override
	protected Class<?>[] getServletConfigClasses() {
		return new Class<?>[] { WebMvcConfig.class };
	}

	@Override
	protected String[] getServletMappings() {
		return new String[] { "/" };
	}

	@Override
	protected Filter[] getServletFilters() {
		// encoding filter
		CharacterEncodingFilter characterEncodingFilter = new CharacterEncodingFilter();
		characterEncodingFilter.setEncoding("UTF-8");

		return new Filter[] { characterEncodingFilter };
	}

	@Override
	protected void customizeRegistration(ServletRegistration.Dynamic registration) {
		MultipartConfigElement multipartConfigElement = new MultipartConfigElement(SpeechToTextUtil.UPLOAD_TMP_DIR.toString());
		registration.setMultipartConfig(multipartConfigElement);
	}

	@Override
	public void onStartup(ServletContext servletContext) throws ServletException {
		super.onStartup(servletContext);

		// Sphinx 4 uses JUL logger, this will bridge/route all JUL log records to the SLF4J API
		SLF4JBridgeHandler.removeHandlersForRootLogger();
		SLF4JBridgeHandler.install();

		// create the temporal directory for uploads
		if (!Files.exists(SpeechToTextUtil.UPLOAD_TMP_DIR)) {
			try {
				Files.createDirectories(SpeechToTextUtil.UPLOAD_TMP_DIR);
			} catch (IOException e) {
				throw new ServletException("Couldn't create temporal directory '" + SpeechToTextUtil.UPLOAD_TMP_DIR + "'", e);
			}
		}
	}

}
