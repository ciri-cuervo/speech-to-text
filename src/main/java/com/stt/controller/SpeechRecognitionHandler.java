package com.stt.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import com.stt.service.SpeechRecognitionService;
import com.stt.service.SpeechRecognizerManager;
import com.stt.service.responsehandler.WebSocketResponseHandler;
import com.stt.util.WebSocketToken;

public class SpeechRecognitionHandler extends TextWebSocketHandler {

	private static final Logger LOGGER = LoggerFactory.getLogger(SpeechRecognitionHandler.class);

	@Autowired
	private WebApplicationContext applicationContext;

	// get prototype bean
	private SpeechRecognitionService getSpeechRecognitionService(WebSocketSession session) {
		return applicationContext.getBean(SpeechRecognitionService.class, new WebSocketResponseHandler(session));
	}

	@Override
	protected void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {
		LOGGER.debug("text message: {} - sessionID: {}", message, session.getId());

		if (WebSocketToken.CREATE_RECOGNIZER.equals(message.getPayload())) {
			// create a new SpeechRecognizer, since it is not thread-safe we must instantiate a new one per thread (session)
			SpeechRecognizerManager.createSpeechRecognizer(session, getSpeechRecognitionService(session));
			LOGGER.info("recognizer count: {} - sessionID: {}", SpeechRecognizerManager.getRecognizerCount(), session.getId());
			return;
		}
	}

	@Override
	public void afterConnectionEstablished(WebSocketSession session) throws Exception {
		LOGGER.debug("connection established - sessionID: {}", session.getId());
		session.sendMessage(new TextMessage(WebSocketToken.SESSION_ID + session.getId()));
	}

	@Override
	public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
		LOGGER.debug("connection closed - sessionID: {}", session.getId());
		SpeechRecognizerManager.deleteSpeechRecognizer(session.getId());
		LOGGER.info("recognizer count: {} - sessionID: {}", SpeechRecognizerManager.getRecognizerCount(), session.getId());
	}

}
