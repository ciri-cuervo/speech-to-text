package com.stt.controller;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.stt.model.SpeechRecognition;
import com.stt.repository.SpeechRecognitionRepository;
import com.stt.repository.UserRepository;
import com.stt.service.SpeechRecognizerManager;
import com.stt.util.SpeechToTextUtil;

/**
 * Handles requests for the application speech upload requests
 * 
 */
@RestController
public class SpeechRecognitionController {

	private static final Logger LOGGER = LoggerFactory.getLogger(SpeechRecognitionController.class);

	private static final String[] AUDIO_FORMATS = { "audio/x-wav", "audio/l16" };
	private static final int BUFFER_SIZE = 50 * 1024; // 50 Kb

	@Autowired
	private SpeechRecognitionRepository speechRecognitionRepository;
	@Autowired
	private UserRepository userRepository;

	/**
	 * Upload single file
	 */
	@RequestMapping(value = "/uploadspeech", method = RequestMethod.POST)
	public ResponseEntity<String> uploadSpeech(@RequestParam MultipartFile speech, HttpServletRequest request) {

		String sessionId = request.getHeader("WebSocketSessionID");

		try {
			LOGGER.info("Starting speech recognition for file: {}", speech.getOriginalFilename());

			// validate parameters
			if (sessionId == null) {
				return new ResponseEntity<String>("Failed to upload speech. Reason: websocket session id is empty", HttpStatus.BAD_REQUEST);
			}
			if (speech.isEmpty()) {
				return new ResponseEntity<String>("Failed to upload speech. Reason: speech file is empty", HttpStatus.BAD_REQUEST);
			}

			String speechName = speech.getOriginalFilename() + "-" + sessionId + "-" + Thread.currentThread().getId() + ".wav";
			Path speechFile = SpeechToTextUtil.storeFileIntoDisk(speechName, speech.getBytes());

			if (SpeechRecognizerManager.execute(sessionId, speechFile)) {
				LOGGER.info("queued tasks: {} - sessionID: {}", SpeechRecognizerManager.getQueuedTaskCount(sessionId), sessionId);
				return new ResponseEntity<String>(HttpStatus.OK);
			}

			LOGGER.warn("Failed to recognize speech. Reason: invalid session id - sessionID: {}", sessionId);
			return new ResponseEntity<String>("Failed to recognize speech. Reason: invalid websocket session id",
					HttpStatus.UNPROCESSABLE_ENTITY);

		} catch (Exception e) {
			LOGGER.error("Error uploading file: {}", speech.getOriginalFilename(), e);
			return new ResponseEntity<String>("Failed to recognize speech. Reason: " + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Upload WAMI WAV file
	 */
	@RequestMapping(value = "/uploadspeech/wami", method = RequestMethod.POST)
	public ResponseEntity<String> uploadWamiSpeech(HttpServletRequest request) {

		String sessionId = request.getHeader("WebSocketSessionID");

		try {
			LOGGER.info("Starting speech upload for WAMI");

			// validate parameters
			if (sessionId == null) {
				return new ResponseEntity<String>("Failed to upload speech. Reason: websocket session id is empty", HttpStatus.BAD_REQUEST);
			}
			if (!Arrays.asList(AUDIO_FORMATS).contains(request.getContentType())) {
				return new ResponseEntity<String>("Failed to upload speech. Reason: content type is not valid", HttpStatus.BAD_REQUEST);
			}

			// get the speech from input stream
			ByteArrayOutputStream os = new ByteArrayOutputStream(BUFFER_SIZE);
			try (InputStream is = request.getInputStream()) {
				copy(is, os, BUFFER_SIZE);
			}

			String speechName = "wabirecorder" + "-" + sessionId + "-" + Thread.currentThread().getId() + ".wav";
			Path speechFile = SpeechToTextUtil.storeFileIntoDisk(speechName, os.toByteArray());

			if (SpeechRecognizerManager.execute(sessionId, speechFile)) {
				LOGGER.info("queued tasks: {} - sessionID: {}", SpeechRecognizerManager.getQueuedTaskCount(sessionId), sessionId);
				return new ResponseEntity<String>(HttpStatus.OK);
			}

			LOGGER.warn("Failed to recognize speech. Reason: invalid session id - sessionID: {}", sessionId);
			return new ResponseEntity<String>("Failed to recognize speech. Reason: invalid websocket session id",
					HttpStatus.UNPROCESSABLE_ENTITY);

		} catch (Exception e) {
			LOGGER.error("Error uploading WAMI file", e);
			return new ResponseEntity<String>("Failed to recognize speech. Reason: " + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/admin/speechrecognitions", method = RequestMethod.GET)
	public ModelAndView listAll() {
		return new ModelAndView("speechrecognitions", "speechrecognitions", speechRecognitionRepository.findAll());
	}

	@RequestMapping(value = "/admin/speechrecognitions/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<SpeechRecognition> delete(@PathVariable Long id) throws IllegalAccessException {
		assertExistsSpeechRecognition(id);
		speechRecognitionRepository.delete(id);
		return new ResponseEntity<SpeechRecognition>(HttpStatus.ACCEPTED);
	}

	@RequestMapping(value = "/admin/speechrecognitions/{id}/wav", method = RequestMethod.GET)
	public void downloadWav(@PathVariable Long id, HttpServletResponse response) throws IllegalAccessException {
		assertExistsSpeechRecognition(id);
		SpeechRecognition recognition = speechRecognitionRepository.findOne(id);
		Path speechPath = Paths.get(recognition.getSpeechPath());

		try {
			byte[] speechFile = Files.readAllBytes(speechPath);

			response.setHeader("Content-Disposition", "attachment; filename=\"" + recognition.getSpeechFilename() + "\"");
			response.setContentLength(speechFile.length);
			response.setContentType("audio/x-wav");

			try (OutputStream os = response.getOutputStream()) {
				os.write(speechFile);
			}
		} catch (IOException e) {
			throw new ResourceAccessException("Resource not found", e);
		}
	}

	@ExceptionHandler(IllegalAccessException.class)
	@ResponseStatus(value = HttpStatus.FORBIDDEN)
	public String handleException(IllegalAccessException ex, HttpServletRequest request) {
		LOGGER.warn(request.getRequestURI(), ex);
		return ex.getMessage();
	}

	@ExceptionHandler(IllegalArgumentException.class)
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	public String handleException(IllegalArgumentException ex, HttpServletRequest request) {
		LOGGER.warn(request.getRequestURI(), ex);
		return ex.getMessage();
	}

	@ExceptionHandler(ResourceAccessException.class)
	@ResponseStatus(value = HttpStatus.NOT_FOUND)
	public String handleException(ResourceAccessException ex, HttpServletRequest request) {
		LOGGER.warn(request.getRequestURI(), ex);
		return ex.getMessage();
	}

	private void assertExistsSpeechRecognition(Long noteId) throws ResourceAccessException {
		if (speechRecognitionRepository.findOne(noteId) == null) {
			throw new ResourceAccessException("Resource not found.");
		}
	}

	/**
	 * 
	 * @param input
	 * @param output
	 * @param bufferSize
	 * @throws IOException
	 */
	private static void copy(InputStream input, OutputStream output, int bufferSize) throws IOException {
		byte[] buffer = new byte[bufferSize];
		int n = 0;
		while (-1 != (n = input.read(buffer))) {
			output.write(buffer, 0, n);
		}
	}

}
