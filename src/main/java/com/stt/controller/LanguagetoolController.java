package com.stt.controller;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.stt.util.HTTPConnector;

/**
 * Works as a proxy between front-end users and languagetool services
 * 
 */
@RestController
public class LanguagetoolController {

	private static final Logger LOGGER = LoggerFactory.getLogger(LanguagetoolController.class);

	private static final int BUFFER_SIZE = 50 * 1024; // 50 Kb

	private static final String UTF8 = HTTPConnector.CHARSET_UNICODE;
	private static final String XML = HTTPConnector.CONTENT_TYPE_XML;

	/**
	 * LanguageTool proxy
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/languagetool", method = RequestMethod.POST, produces = { XML })
	public ResponseEntity<String> uploadWamiSpeech(HttpServletRequest request) {

		try {
			LOGGER.info("Starting languagetool service: " + request.getHeader("Content-type"));

			// get the text from input stream
			ByteArrayOutputStream os = new ByteArrayOutputStream(BUFFER_SIZE);
			try (InputStream is = request.getInputStream()) {
				copy(is, os, BUFFER_SIZE);
			}

			String checked = HTTPConnector.post("https://languagetool.org:8081")
				.setOutput(os.toByteArray(), request.getHeader("Content-type"))
				.setHeader("Referer", request.getHeader("Referer"))
				.executeString(30000, UTF8);

			return new ResponseEntity<String>(checked, HttpStatus.OK);

		} catch (Exception e) {
			LOGGER.error("Error using languagetool service", e);
			return new ResponseEntity<String>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * 
	 * @param input
	 * @param output
	 * @param bufferSize
	 * @throws IOException
	 */
	private static void copy(InputStream input, OutputStream output, int bufferSize) throws IOException {
		byte[] buffer = new byte[bufferSize];
		int n = 0;
		while (-1 != (n = input.read(buffer))) {
			output.write(buffer, 0, n);
		}
	}

}
