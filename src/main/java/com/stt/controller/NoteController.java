package com.stt.controller;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.servlet.ModelAndView;

import com.stt.model.Note;
import com.stt.model.User;
import com.stt.repository.NoteRepository;
import com.stt.repository.UserRepository;

@RestController
public class NoteController {

	private static final Logger LOGGER = LoggerFactory.getLogger(NoteController.class);

	@Autowired
	private NoteRepository noteRepository;
	@Autowired
	private UserRepository userRepository;

	@RequestMapping(value = "/notes", method = RequestMethod.GET)
	public ModelAndView myNotes() {
		User user = getLoggedUser();
		return new ModelAndView("notes", "notes", noteRepository.findByUserId(user.getId()));
	}

	@RequestMapping(value = "/notes/{id}", method = RequestMethod.GET)
	public ModelAndView get(@PathVariable Long id) throws IllegalAccessException {
		assertExistsNote(id);
		assertLoggedUserIsOwner(id);
		return new ModelAndView("index", "note", noteRepository.findOne(id));
	}

	@RequestMapping(value = "/notes/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Note> delete(@PathVariable Long id) throws IllegalAccessException {
		assertExistsNote(id);
		assertLoggedUserIsOwner(id);
		noteRepository.delete(id);
		return new ResponseEntity<Note>(HttpStatus.ACCEPTED);
	}

	@RequestMapping(value = "/notes", method = RequestMethod.POST)
	public ResponseEntity<Note> create(@RequestBody Note note, HttpServletRequest request) {
		note.setId(0L);
		note.setUser(getLoggedUser());
		note.setCreated(new Date());
		note.setModified(new Date());
		note = noteRepository.save(note);

		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.set("Location", request.getRequestURI() + "/" + note.getId());
		return new ResponseEntity<Note>(responseHeaders, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/notes/{id}", method = RequestMethod.PATCH)
	public ResponseEntity<Note> update(@PathVariable Long id, @RequestBody Note patch, HttpServletRequest request)
			throws IllegalAccessException {
		assertExistsNote(id);
		assertLoggedUserIsOwner(id);

		Note note = noteRepository.findOne(id);
		note.setTitle(patch.getTitle());
		note.setText(patch.getText());
		note.setModified(new Date());
		note = noteRepository.save(note);

		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.set("Location", request.getRequestURI());
		return new ResponseEntity<Note>(responseHeaders, HttpStatus.NO_CONTENT);
	}

	@ExceptionHandler(IllegalAccessException.class)
	@ResponseStatus(value = HttpStatus.FORBIDDEN)
	public String handleException(IllegalAccessException ex, HttpServletRequest request) {
		LOGGER.warn(request.getRequestURI(), ex);
		return ex.getMessage();
	}

	@ExceptionHandler(IllegalArgumentException.class)
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	public String handleException(IllegalArgumentException ex, HttpServletRequest request) {
		LOGGER.warn(request.getRequestURI(), ex);
		return ex.getMessage();
	}

	@ExceptionHandler(ResourceAccessException.class)
	@ResponseStatus(value = HttpStatus.NOT_FOUND)
	public String handleException(ResourceAccessException ex, HttpServletRequest request) {
		LOGGER.warn(request.getRequestURI(), ex);
		return ex.getMessage();
	}

	private void assertExistsNote(Long noteId) throws ResourceAccessException {
		if (noteRepository.findOne(noteId) == null) {
			throw new ResourceAccessException("Resource not found.");
		}
	}

	private void assertLoggedUserIsOwner(Long noteId) throws IllegalAccessException {
		User user = getLoggedUser();
		Note note = noteRepository.findOne(noteId);
		if (!user.equals(note.getUser())) {
			throw new IllegalAccessException("You don't have permission to access this resource.");
		}
	}

	private User getLoggedUser() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String username = auth.getName(); // get logged-in username
		return userRepository.findByUsername(username);
	}

}
