
var savingNote = false;

function saveNote() {
	// check if already saving, not to overload the server
	if (savingNote) {
		return false;
	}
	savingNote = true;

	var type = (noteLocation) ? 'PATCH' : 'POST';
	var url = noteLocation || cp + '/notes';
	var note = $('.mceEditor iframe').contents().find('#tinymce');

	$.ajax({
		url : url,
		type : type,
		data : JSON.stringify({ title : $('#title').val(), text : note.html() }),
		contentType : 'application/json',
		beforeSend : function(xhr) {
			xhr.setRequestHeader("X-CSRF-TOKEN", csrf_token);
		}
	}).done(function(data, textStatus, jqXHR) {
		console.log('Note saved');
		noteLocation = jqXHR.getResponseHeader('Location');
	}).fail(function(jqXHR, textStatus, errorThrown) {
		console.log('Note not saved: ' + errorThrown);
	}).always(function() {
		savingNote = false;
	});

	return true;
}

function deleteNote(noteId) {
	if (!confirm('Do you really want to delete this note?')) {
		return;
	}
	$.ajax({
		url : cp + "/notes/" + noteId,
		type : 'DELETE',
		beforeSend : function(xhr) {
			xhr.setRequestHeader('X-CSRF-TOKEN', csrf_token);
		}
	}).done(function(data, textStatus, jqXHR) {
		console.log('Note deleted');
		$('div #note-' + noteId).remove();
	}).fail(function(jqXHR, textStatus, errorThrown) {
		alert('Note not deleted: ' + errorThrown);
	}).always(function() {
	});
}

function deleteSpeechrecognition(srId) {
	if (!confirm('Do you really want to delete this speech?')) {
		return;
	}
	$.ajax({
		url : cp + "/admin/speechrecognitions/" + srId,
		type : 'DELETE',
		beforeSend : function(xhr) {
			xhr.setRequestHeader('X-CSRF-TOKEN', csrf_token);
		}
	}).done(function(data, textStatus, jqXHR) {
		console.log('Speech deleted');
		$('div #speechrecognition-' + srId).remove();
	}).fail(function(jqXHR, textStatus, errorThrown) {
		alert('Speech not deleted: ' + errorThrown);
	}).always(function() {
	});
}

function uploadFile(blobFile, fileName) {
    var fd = new FormData();
    fd.append('speech', blobFile, fileName);

	startRecognizingIcon();

	$.ajax({
		url : cp + '/uploadspeech',
		type : "POST",
		data : fd,
		processData : false,
		contentType : false,
		beforeSend : function(xhr) {
			xhr.setRequestHeader('X-CSRF-TOKEN', csrf_token);
			xhr.setRequestHeader('WebSocketSessionID', websocketSession);
		}
	}).done(function(data, textStatus, jqXHR) {
		console.log('Speech uploaded');
	}).fail(function(jqXHR, textStatus, errorThrown) {
		alert('Speech not uploaded: ' + errorThrown);
		stopRecognizingIcon();
	}).always(function() {
	});
}

function appendRecognition(text) {
	// check if it's an empty message
	if ($.trim(text).length == 0) {
		return;
	}

	// append the text to textarea
	var note = $('.mceEditor iframe').contents().find('#tinymce');
	var lastParagraph = note.find('p:last-child');
	if (lastParagraph.length > 0 && $.trim(lastParagraph.text()).length == 0) {
		lastParagraph.html(text);
	} else {
		note.append($('<p></p>').append(text));
	}
	note.scrollTop(note.prop("scrollHeight"));
	//if ($.trim(note.val()).length > 0) {
	//	note.val(note.val() + '\n');
	//}
	//note.val(note.val() + text);
	//note.trigger('propertychange');
}

function micIsEnabled() {
	$('#record').prop('disabled', false);
	micIsNotActive();
}

function micIsDisabled() {
	$('#record').prop('disabled', true);
	micIsNotActive();
}

function micIsActive() {
	$('#record > i').removeClass('fa-microphone-slash').addClass('fa-microphone');
}

function micIsNotActive() {
	$('#record > i').addClass('fa-microphone-slash').removeClass('fa-microphone');
	micIsNotRecording();
}

function micIsRecording() {
	micIsActive();
	$('#record > i').addClass('recording');
}

function micIsNotRecording() {
	$('#record > i').removeClass('recording');
}

var recognizingCount = 0;

function startRecognizingIcon() {
	recognizingCount++;
	$('#loading').removeClass('hide');
}

function stopRecognizingIcon() {
	recognizingCount = Math.max(--recognizingCount, 0);
	if (recognizingCount == 0) {
		$('#loading').addClass('hide');
	}
}

function initSpeechRecognition() {

	// webkit shim
	window.AudioContext = window.AudioContext
			|| window.webkitAudioContext;

	window.SpeechRecognition = window.SpeechRecognition
			|| window.webkitSpeechRecognition;

	navigator.getUserMedia = navigator.getUserMedia
			|| navigator.webkitGetUserMedia
			|| navigator.mozGetUserMedia
			|| navigator.msGetUserMedia;

	var flash_installed = ((typeof navigator.plugins != "undefined" && typeof navigator.plugins["Shockwave Flash"] == "object")
			|| (window.ActiveXObject && (new ActiveXObject("ShockwaveFlash.ShockwaveFlash")) != false));

	if (!window.SpeechRecognition) {
		if (!window.AudioContext || !navigator.getUserMedia) {
			if (!flash_installed) {
				throw 'No audio input detected in this browser!';
			} else {
				wami_setUpRecognition(); // SWF recording
			}
		} else {
			audio_context = new window.AudioContext(); // Web Audio API
			try {
				webSocketConnect();
			} catch (e) {
				throw 'No web socket support in this browser!';
			}
		}
	} else {
		speech_recognition = new window.SpeechRecognition(); // Web Speech API
		wsa_setUpRecognition();
	}

	console.log('Audio context set up.');
}
