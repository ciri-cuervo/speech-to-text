function lt_init() {
	tinyMCE.init({
		mode : "textareas",
		plugins : "AtD,paste",
		paste_text_sticky : true,
		setup : function(ed) {
			ed.onInit.add(function(ed) {
				ed.pasteAsPlainText = true;
			});
		},

		/* translations: */
		languagetool_i18n_no_errors : {
			// "No errors were found.":
			"de-DE" : "Keine Fehler gefunden."
		},
		languagetool_i18n_explain : {
			// "Explain..." - shown if there is an URL with a detailed
			// description:
			"de-DE" : "Mehr Informationen..."
		},
		languagetool_i18n_ignore_once : {
			// "Ignore this error":
			"de-DE" : "Hier ignorieren"
		},
		languagetool_i18n_ignore_all : {
			// "Ignore this kind of error":
			"de-DE" : "Fehler dieses Typs ignorieren"
		},
		languagetool_i18n_rule_implementation : {
			// "Rule implementation":
			"de-DE" : "Implementierung der Regel"
		},

		languagetool_i18n_current_lang : function() {
			//return document.checkform.lang.value;
			return "en-US";
		},

		/* the URL of your proxy file - must be hosted on your domain: */
		languagetool_rpc_url : cp + "/languagetool",
		/* edit this file to customize how LanguageTool shows errors: */
		languagetool_css_url : cp + "/resources/css/languagetool.css",
		/* this stuff is a matter of preference: */
		theme : "advanced",
		theme_advanced_buttons1 : "",
		theme_advanced_buttons2 : "",
		theme_advanced_buttons3 : "",
		theme_advanced_toolbar_location : "none",
		theme_advanced_toolbar_align : "left",
		theme_advanced_statusbar_location : "bottom",
		theme_advanced_path : false,
		theme_advanced_resizing : true,
		theme_advanced_resizing_use_cookie : false,
		gecko_spellcheck : false
	});
}

function doit() {
	//var langCode = document.checkform.lang.value;
	tinyMCE.activeEditor.execCommand("mceWritingImprovementTool", "en-US");
}
