var speech_recognition, wsa_recognizing = false, wsa_ignore_onend, wsa_final_transcript = '', wsa_start_timestamp;

function wsa_setUpRecognition() {

	micIsEnabled();
	speech_recognition.continuous = true;
	speech_recognition.interimResults = true;

	speech_recognition.onstart = function() {
		wsa_recognizing = true;
		micIsRecording();
		openInterimWindow();
	};

	speech_recognition.onerror = function(event) {
		if (event.error == 'no-speech') {
			wsa_ignore_onend = true;
		}
		if (event.error == 'audio-capture') {
			wsa_ignore_onend = true;
		}
		if (event.error == 'not-allowed') {
			wsa_ignore_onend = true;
			if (event.timeStamp - wsa_start_timestamp < 100) {
				// blocked
			} else {
				// denied
			}
		}
		console.log('There was an error: ' + event.error);
	};

	speech_recognition.onend = function() {
		stopRecognizingIcon();
		micIsNotActive();
		wsa_recognizing = false;
		if (wsa_ignore_onend) {
			return;
		}
		if (wsa_final_transcript) {
			appendRecognition(wsa_final_transcript);
			doit(); // trigger automated spell-check
		}
		closeInterimWindow();
	};

	speech_recognition.onresult = function(event) {
		var interim_transcript = '';
		if (typeof (event.results) == 'undefined') {
			speech_recognition.onend = null;
			speech_recognition.stop();
			console.log('There was an error, undefined results.');
			return;
		}
		for (var i = event.resultIndex; i < event.results.length; ++i) {
			if (event.results[i].isFinal) {
				wsa_final_transcript += event.results[i][0].transcript + '\n';
			} else {
				interim_transcript += event.results[i][0].transcript;
			}
		}
		if (wsa_final_transcript) {
			appendFinalInterim();
			appendInterim('');
		}
		if (interim_transcript) {
			appendInterim(interim_transcript);
		}
	};
}

function wsa_startButton(event) {
	if (wsa_recognizing) {
		startRecognizingIcon();
		speech_recognition.stop();
		return;
	}
	wsa_final_transcript = '';
	speech_recognition.lang = 'en-US';
	speech_recognition.start();
	wsa_ignore_onend = false;

	wsa_start_timestamp = event.timeStamp;
}

function appendFinalInterim() {
	var text = linebreak(wsa_final_transcript);
	$('#interim-final-transcript').html(text).removeClass('hide');
}

function appendInterim(interim_transcript) {
	$('#interim-transcript').html(interim_transcript);
}

function openInterimWindow() {
	$('#interim-message').fadeIn(function() {
		
	});
}

function closeInterimWindow() {
	$('#interim-message').fadeOut(function() {
		$('#interim-transcript').html('');
		$('#interim-final-transcript').html('').addClass('hide');
	});
}

function linebreak(s) {
	return s.replace(/(?:\r\n|\r|\n)/g, '<br />');
}
