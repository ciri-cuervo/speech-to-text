
var sock = null, websocketSession = null;

function webSocketConnect() {

	sock = new SockJS(cp + '/speechrecognition');

	sock.onopen = function() {
		console.log('Open socket connection');
		sock.send('###CREATE###');
	};

	sock.onmessage = function(msg) {

		if (msg.data.startsWith('###ID###')) {
			websocketSession = msg.data.substr(8);
			return;
		}

		if (msg.data == '###READY###') {
			console.log('Recognition service is ready');
			micIsEnabled();
			return;
		}

		if (msg.data == '###END###') {
			console.log('Recognition end');
			stopRecognizingIcon();
			doit(); // trigger automated spell-check
			return;
		}

		console.log('Recognized text: ' + msg.data);

		appendRecognition(msg.data);
	};

	sock.onclose = function() {
		console.log('Close socket connection');
		micIsDisabled();
	};

}

function sendWebSocketMsg(message) {
	try {
		if (sock) {
			sock.send(message);
		} else {
			alert('Error sending websocket message - websocket not connected');
		}
	} catch (e) {
		alert('Error sending websocket message - ' + e);
	}
}

if (typeof String.prototype.startsWith != 'function') {
	String.prototype.startsWith = function(str) {
		return this.slice(0, str.length) == str;
	};
}

if (typeof String.prototype.endsWith != 'function') {
	String.prototype.endsWith = function(str) {
		return this.slice(-str.length) == str;
	};
}
