var audio_context, waa_audioInput, waa_recorder, waa_recording = false;

function waa_startUserMedia(stream) {
	waa_audioInput = audio_context.createMediaStreamSource(stream);
	console.log('Media stream created.');

	// commented not to listen mic through speakers
	//audioInput.connect(audio_context.destination);
	//console.log('Input connected to audio context destination.');

	waa_recorder = new Recorder(waa_audioInput, {
		workerPath : cp + '/resources/js/recorderjs/recorderWorker.js'
	});
	console.log('Recorder initialised.');

	waa_recording = true;
	micIsRecording();
	waa_recorder.clear();
	waa_recorder.record();
}

function waa_startButton() {
	if (waa_recording) {
		waa_recording = false;
		waa_recorder.stop();
		micIsNotRecording();
		waa_uploadWAV();
	} else {
		if (!waa_recorder) {
			navigator.getUserMedia({ audio : true }, waa_startUserMedia, function(e) {
				alert('No live audio input.');
			});
		} else {
			waa_recording = true;
			micIsRecording();
			waa_recorder.clear();
			waa_recorder.record();
		}
	}
}

function waa_uploadWAV() {
	waa_recorder.exportMonoWAV(function(blob) {
		uploadFile(blob, 'webaudioapi');
	}, 16000, 'one-sample'); // downsampling approaches: avg-sample, one-sample
}
