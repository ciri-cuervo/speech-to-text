var wami_enabled = false, wami_recording = false;

function wami_startButton(event) {
	if (wami_recording) {
		wami_recording = false;
		micIsNotRecording();
		Wami.stopRecording();
		startRecognizingIcon();
	} else {
		wami_recording = true;
		micIsRecording();
		Wami.startRecording(cp + '/uploadspeech/wami', csrf_token, websocketSession);
	}
}

function wami_setUpRecognition() {

	$('#wami').parent().removeClass('hide');

	Wami.setup({
		id : 'wami',
		swfUrl : cp + '/resources/js/wami/Wami.swf',
		noSecurityRecheck : true,
		onReady : function() {
			Wami.hide();
			wami_enabled = true;
			try {
				webSocketConnect();
			} catch (e) {
				alert('No web socket support in this browser!');
			}
		},
		onLoaded : function() {
			Wami.setSettings({
				'sampleRate' : 16000
			});
		}
	});
}
