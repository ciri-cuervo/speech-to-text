<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page session="true" trimDirectiveWhitespaces="true" %>
<!DOCTYPE html>
<html lang="en-US">
<head>
<title>Speech To Text</title>
<meta charset="UTF-8">
<link rel="icon" type="image/x-icon" href="${pageContext.request.contextPath}/favicon.ico">

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/swfobject/2.2/swfobject.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<script src="https://cdn.jsdelivr.net/sockjs/0.3.4/sockjs.min.js"></script>
<script src="https://languagetool.org/online-check/tiny_mce/tiny_mce.js"></script>
<script src="https://languagetool.org/online-check/tiny_mce/plugins/atd-tinymce/editor_plugin.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/recorderjs/recorder.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/wami/recorder.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/speechtotext.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/webSocketConnect.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/webAudioApi.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/webSpeechApi.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/wamiRecorderConfig.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/languagetoolConfig.js"></script>
</head>

<body>
	<form action="${pageContext.request.contextPath}/logout" method="post" id="logoutForm">
		<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
	</form>
