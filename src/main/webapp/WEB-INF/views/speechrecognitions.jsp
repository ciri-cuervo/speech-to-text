<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page session="true" trimDirectiveWhitespaces="true" %>
<% pageContext.setAttribute("newLineChar", "\n"); %>
<jsp:include page="header.jsp" />

	<div class="container-fluid">
		<h5 class="pull-right">
			Welcome : ${pageContext.request.userPrincipal.name} |
			<a href="${pageContext.request.contextPath}/">New note</a> |
			<a href="${pageContext.request.contextPath}/notes">My notes</a> |
			<a href="javascript:$('#logoutForm').submit()">Logout</a>
		</h5>
	</div>

	<div class="container-fluid">
		<c:forEach items="${speechrecognitions}" var="speechrecognition">
			<div class="row" id="speechrecognition-${speechrecognition.id}">
				<div class="col-lg-6">
					<div class="panel panel-warning">
						<div class="panel-heading">
							<h5 class="panel-title">
								<c:choose>
									<c:when test="${not empty speechrecognition.speechFilename}">${speechrecognition.speechFilename}</c:when>
									<c:otherwise>&nbsp;</c:otherwise>
								</c:choose>
								<small class="pull-right">
									<a href="${pageContext.request.contextPath}/admin/speechrecognitions/${speechrecognition.id}/wav"
											target="_blank">Download WAV</a> |
									<a href="javascript:void(0)" onclick="deleteSpeechrecognition(${speechrecognition.id})">Delete</a>
								</small>
							</h5>
						</div>
						<div class="panel-body">
							<c:choose>
								<c:when test="${not empty speechrecognition.text}">${fn:replace(speechrecognition.text, newLineChar, "<br />")}</c:when>
								<c:otherwise>&nbsp;</c:otherwise>
							</c:choose>
						</div>
						<c:if test="${not empty speechrecognition.error}">
							<div class="panel-footer">
								${fn:replace(speechrecognition.error, newLineChar, "<br />")}
							</div>
						</c:if>
					</div>
				</div>
			</div>
		</c:forEach>
	</div>

<jsp:include page="footer.jsp" />
