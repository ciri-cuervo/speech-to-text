<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page session="true" trimDirectiveWhitespaces="true" %>
<c:set var="now" value="<%=new java.util.Date()%>" />
<jsp:include page="header.jsp" />

	<div class="container-fluid">
		<h5 class="pull-right">
			Welcome : ${pageContext.request.userPrincipal.name} |
			<a href="${pageContext.request.contextPath}/notes">My notes</a> |
			<sec:authorize access="hasRole('ROLE_ADMIN')">
				<a href="${pageContext.request.contextPath}/admin/speechrecognitions">Recognitions</a> |
			</sec:authorize>
			<a href="javascript:$('#logoutForm').submit()">Logout</a>
		</h5>
	</div>

	<div class="container-fluid">
		<form role="form" name="checkform">
			<div class="row">
				<div class="form-group col-lg-8">
					<button type="button" id="save" class="btn btn-primary btn-xs btn-lg-padding">
						<strong>Save</strong></button>
					<button type="button" id="discard" class="btn btn-default btn-xs btn-md-padding">
						<strong>Discard changes</strong></button>
					<button type="button" id="record" class="btn btn-default btn-sm pull-right" disabled>
						<i class="fa fa-microphone-slash fa-lg"></i></button>
					<img id="loading" class="icon hide pull-right" alt="Loading"
						src="${pageContext.request.contextPath}/resources/img/loading.gif" />
					<hr />
				</div>
			</div>
			<div class="row hide">
				<div id="wami" class="col-lg-2 col-md-offset-3"></div>
			</div>
			<div class="row">
				<div class="form-group col-lg-4">
					<input type="text" id="title" class="form-control" placeholder="Subject" value="${note.title}" />
				</div>
				<div class="form-group col-lg-4">
					<small class="pull-right pull-bottom">
						Created on: <fmt:formatDate value="${not empty note.created ? note.created : now}" pattern="yyyy-MM-dd" />
					</small>
				</div>
			</div>
			<div id="interim-message" class="row">
				<div class="form-group col-lg-8">
					<div class="panel panel-info">
						<div class="panel-heading">
							<h5 class="panel-title">Interim recognition</h5>
						</div>
						<div class="panel-body">
							<p id="interim-final-transcript" class="hide"></p>
							<p id="interim-transcript"></p>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="form-group col-lg-8">
					<textarea id="text" class="form-control" rows="10" placeholder="Enter note details here...">${note.text}</textarea>
				</div>
			</div>
			<div class="row">
				<div class="form-group col-lg-8">
					<input type="submit" name="_action_checkText" value="Check Text" onClick="doit();return false;" class="btn btn-primary btn-xs btn-lg-padding pull-right">
				</div>
			</div>
		</form>
	</div>

	<c:if test="${not empty note.id}">
		<c:set var="noteLocation" value="${pageContext.request.contextPath}/notes/${note.id}" />
	</c:if>

	<script type="text/javascript">
		var noteLocation = '${noteLocation}';
		$(document).ready(function() {
			// initialize languagetool
			lt_init();

			try {
				initSpeechRecognition();
			} catch (e) {
				alert('Speech to text not enabled: ' + e);
			}

			$('#save').click(function() {
				// validate data
				if ($.trim($('#title').val()).length == 0) {
					$('#title').parent().addClass('has-error');
					$('#title').focus();
				} else {
					$('#title').parent().removeClass('has-error');
					saveNote();
				}
			});

			$('#discard').click(function() {
				window.location.replace('${pageContext.request.contextPath}/');
			});

			$('#record').click(function(e) {
				if (speech_recognition) {
					wsa_startButton(e);
				} else if (audio_context) {
					waa_startButton(e);
				} else if (wami_enabled) {
					wami_startButton(e);
				}
			});

			$('#title').blur(function() {
				$(this).parent().removeClass('has-error');
			});

			// prevent submit form when pressing enter key
			$(window).keydown(function(e) {
				var code = e.keyCode || e.which || e.charCode || 0; 
				if (code == 13) {
					e.preventDefault();
					return false;
				}
			});
		});
	</script>

<jsp:include page="footer.jsp" />
