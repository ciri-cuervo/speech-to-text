<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page session="true" trimDirectiveWhitespaces="true" %>
<% pageContext.setAttribute("newLineChar", "\n"); %>
<jsp:include page="header.jsp" />

	<div class="container-fluid">
		<h5 class="pull-right">
			Welcome : ${pageContext.request.userPrincipal.name} |
			<a href="${pageContext.request.contextPath}/">New note</a> |
			<sec:authorize access="hasRole('ROLE_ADMIN')">
				<a href="${pageContext.request.contextPath}/admin/speechrecognitions">Recognitions</a> |
			</sec:authorize>
			<a href="javascript:$('#logoutForm').submit()">Logout</a>
		</h5>
	</div>

	<div class="container-fluid">
		<c:forEach items="${notes}" var="note">
			<div class="row" id="note-${note.id}">
				<div class="col-lg-6">
					<div class="panel panel-info">
						<div class="panel-heading">
							<h5 class="panel-title">
								<c:choose>
									<c:when test="${not empty note.title}">${note.title}</c:when>
									<c:otherwise>&nbsp;</c:otherwise>
								</c:choose>
								<small class="pull-right">
									<a href="${pageContext.request.contextPath}/notes/${note.id}">Edit</a> |
									<a href="javascript:void(0)" onclick="deleteNote(${note.id})">Delete</a>
								</small>
							</h5>
						</div>
						<div class="panel-body">
							<c:choose>
								<c:when test="${not empty note.text}">${fn:replace(note.text, newLineChar, "<br />")}</c:when>
								<c:otherwise>&nbsp;</c:otherwise>
							</c:choose>
						</div>
					</div>
				</div>
			</div>
		</c:forEach>
	</div>

<jsp:include page="footer.jsp" />
