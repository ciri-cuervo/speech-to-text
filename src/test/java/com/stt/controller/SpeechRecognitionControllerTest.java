package com.stt.controller;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.stt.config.*;
import com.stt.util.SpeechToTextUtil;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { WebMvcConfig.class, PersistenceConfig.class, SpeechRecognizerConfig.class, TestConfig.class })
@WebAppConfiguration
public class SpeechRecognitionControllerTest {

	private MockMvc mockMvc;
	private byte[] speech1;

	@Autowired
	private WebApplicationContext webApplicationContext;

	@Before
	public void setUp() throws IOException, URISyntaxException {
		mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
		URL speech1Url = SpeechRecognitionControllerTest.class.getResource("10001-90210-01803.wav");
		speech1 = Files.readAllBytes(Paths.get(speech1Url.toURI()));
	}

	@Test
	public void testSpeechUpload() throws Exception {

		MockMultipartFile speechFile = new MockMultipartFile("speech", "10001-90210-01803.wav", "application/x-wav", speech1);

		mockMvc.perform(MockMvcRequestBuilders.fileUpload("/upload-speech")
					.file(speechFile))
				.andExpect(status().is(200))
				.andExpect(content().contentType(SpeechToTextUtil.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.text", is("one zero zero zero one\nnine oh two one oh\ncyril one eight zero three")))
                .andExpect(jsonPath("$.speechFilename", is("10001-90210-01803.wav")));

	}
}
