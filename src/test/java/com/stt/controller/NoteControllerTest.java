package com.stt.controller;

import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.stt.config.*;
import com.stt.model.Note;
import com.stt.model.User;
import com.stt.repository.NoteRepository;
import com.stt.repository.UserRepository;
import com.stt.util.SpeechToTextUtil;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { WebMvcConfig.class, PersistenceConfig.class, SpeechRecognizerConfig.class, TestConfig.class })
@WebAppConfiguration
public class NoteControllerTest {

	private MockMvc mockMvc;

	@Autowired
	private WebApplicationContext webApplicationContext;
	@Autowired
    private NoteRepository noteRepository;
	@Autowired
    private UserRepository userRepository;

	private User user;

	@Before
	public void setUp() throws Exception {
		mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();

		user = new User();
		user.setUsername("test");
		user.setPassword("test");
		user.setEmail("test@test.com");
		user = userRepository.save(user);

		org.springframework.security.core.userdetails.User userCred = new org.springframework.security.core.userdetails.User(
				user.getUsername(), user.getPassword(), new ArrayList<GrantedAuthority>());
		SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(userCred, userCred.getPassword()));

	}

	@After
	public void tearDown() throws Exception {
		SecurityContextHolder.clearContext();
		userRepository.delete(this.user);
	}

	@Test
	public void testSave() throws Exception {
		ObjectMapper objectMapper = new ObjectMapper();

		Note note = new Note();
		note.setTitle("Note title");
		note.setText("Note text");

		mockMvc.perform(post("/notes", note)
				.contentType(SpeechToTextUtil.APPLICATION_JSON_UTF8)
		        .content(objectMapper.writeValueAsString(note)))
			.andExpect(status().isCreated());

	}
}
