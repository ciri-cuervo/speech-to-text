# Speech To Text #

A web application for speech recognition in real time.

It uses in combination the Google Chrome **Web Speech API** and the **CMU Sphinx4** library for a high accurate translation.
